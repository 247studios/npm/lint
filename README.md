# @247studios/lint

Code quality tools for 247studios projects.

`npm install --save-dev @247studios/lint`

# Usage

```sh
lint prettier base www/src/client && lint eslint browser www/src/client
lint prettier base www/src/server && lint eslint node www/src/server
lint prettier base api/src && lint eslint node api/src
```

# Releasing New Versions

Specify semantic version change by running one of `npm version patch`,
`npm version minor`, or `npm version major`.

Once commit reaches the master branch on GitLab, the release will be deployed
to npm automatically.
