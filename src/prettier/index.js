/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */
const child_process = require(`child_process`);
const fs = require(`fs`);
const path = require(`path`);

module.exports = function prettier(files, options) {
  return new Promise((resolve) => {
    if (files.length === 0) {
      return;
    }
    const configPath = path.join(__dirname, `config/${options.config}.js`);
    const cwdPath = process.cwd();
    const ignorePath = path.join(cwdPath, `.gitignore`);
    const PATH = [
      path.join(__dirname, `../../node_modules/.bin`),
      process.env.PATH,
    ].join(`:`);
    try {
      fs.statSync(configPath);
    } catch (error) {
      if (error.code === `ENOENT`) {
        resolve({
          error,
          stdout: ``,
          stderr: `"${options.config}" is not a valid prettier configuration name.\n`,
        });
      } else {
        resolve({ error, stdout: ``, stderr: `` });
      }
      return;
    }
    const command = [
      `prettier`,
      `--config`,
      configPath,
      `--ignore-path`,
      ignorePath,
      options.fix ? `--write` : `--check`,
      ...files.map((file) => path.join(cwdPath, file)),
    ]
      .filter(Boolean)
      .map((x) => `"${x}"`)
      .join(` `);
    child_process.exec(
      command,
      {
        cwd: cwdPath,
        env: {
          PATH,
        },
      },
      (error, stdout, stderr) => {
        resolve({ error, stdout, stderr });
      }
    );
  });
};
