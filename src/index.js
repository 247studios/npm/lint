#!/usr/bin/env node
/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */

const { program } = require(`commander`);
const eslint = require(`./eslint`);
const prettier = require(`./prettier`);

program
  .command(`eslint <config> [files...]`)
  .option(`--fix`, `Automatically fix problems`)
  .action((config, files, options) => {
    eslint(files, {
      config,
      fix: options.fix,
    }).then(({ error, stdout, stderr }) => {
      if (stdout != null) {
        process.stdout.write(stdout);
      }
      if (stderr != null) {
        process.stderr.write(stderr);
      }
      if (error != null) {
        process.exitCode = 1;
      }
    });
  });

program
  .command(`prettier <config> [files...]`)
  .option(`--fix`, `Automatically fix problems`)
  .action((config, files, options) => {
    prettier(files, {
      config,
      fix: options.fix,
    }).then(({ error, stdout, stderr }) => {
      if (stdout != null) {
        process.stdout.write(stdout);
      }
      if (stderr != null) {
        process.stderr.write(stderr);
      }
      if (error != null) {
        process.exitCode = 1;
      }
    });
  });

program.parse(process.argv);
