/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */
const OPERATORS = {
  before: (length, index) => index,
  after: (length, index) => index + 1,
  start: (length, index) => 0,
  end: (length, index) => length,
};

/**
 * @example
 * > insert([1, 2, 3], [[4, "after", 2]])
 * [ 1, 2, 4, 3 ]
 * > insert([1, 2, 3], [[4, "before", 2]])
 * [ 1, 4, 2, 3 ]
 * > insert([1, 2, 3], [[4, "start"]])
 * [ 4, 1, 2, 3 ]
 * > insert([1, 2, 3], [[4, "end"]])
 * [ 1, 2, 3, 4 ]
 */
module.exports = function insert(array, ops) {
  return ops.reduce((result, op) => {
    const [lhs, operator, rhs] = op;
    const operatorOffset = OPERATORS[operator];
    if (operatorOffset == null) {
      throw new Error(
        `Expected ${JSON.stringify(operator)} to be one of ${JSON.stringify(
          OPERATORS
        )}`
      );
    }
    const rhsIndex = result.indexOf(rhs);
    const sliceIndex = operatorOffset(result.length, rhsIndex);
    if (sliceIndex === -1) {
      throw new Error(
        `Expected ${JSON.stringify(result)} to contain ${JSON.stringify(rhs)}`
      );
    }
    return [
      ...result.slice(0, sliceIndex),
      ...(Array.isArray(lhs) ? lhs : [lhs]),
      ...result.slice(sliceIndex),
    ];
  }, array);
};
