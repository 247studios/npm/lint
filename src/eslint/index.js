/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */

const child_process = require(`child_process`);
const fs = require(`fs`);
const path = require(`path`);

module.exports = function eslint(files, options) {
  return new Promise((resolve) => {
    if (files.length === 0) {
      return;
    }
    const rootPath = path.resolve(__dirname, `../..`);
    const cwdPath = process.cwd();
    const configPath = path.resolve(__dirname, `config/${options.config}.js`);
    const ignorePath = path.resolve(cwdPath, `.gitignore`);
    const PATH = [
      path.resolve(rootPath, `node_modules/.bin`),
      process.env.PATH,
    ].join(`:`);
    const ext = `js,jsx,json,ts,tsx`;
    try {
      fs.statSync(configPath);
    } catch (error) {
      if (error.code === `ENOENT`) {
        resolve({
          error,
          stdout: ``,
          stderr: `"${options.config}" is not a valid eslint configuration name.\n`,
        });
      } else {
        resolve({ error, stdout: ``, stderr: `` });
      }
      return;
    }
    const command = [
      `eslint`,
      `--config`,
      configPath,
      `--ignore-path`,
      ignorePath,
      `--ext`,
      ext,
      `--resolve-plugins-relative-to`,
      rootPath,
      options.fix && `--fix`,
      ...files.map((file) => path.resolve(cwdPath, file)),
    ]
      .filter(Boolean)
      .map((x) => `"${x}"`)
      .join(` `);
    child_process.exec(
      command,
      {
        cwd: cwdPath,
        env: {
          PATH,
        },
      },
      (error, stdout, stderr) => {
        resolve({ error, stdout, stderr });
      }
    );
  });
};
