/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */

const node = require(`./node.js`);
const insert = require(`../utilities/insert`);

module.exports = {
  ...node,
  parser: `@typescript-eslint/parser`,
  extends: insert(node.extends, [
    [
      [
        `plugin:@typescript-eslint/eslint-recommended`,
        `plugin:@typescript-eslint/recommended`,
      ],
      `after`,
      `eslint:recommended`,
    ],
  ]),
  plugins: insert(node.plugins, [
    [`@typescript-eslint/eslint-plugin`, `start`],
  ]),
  settings: {
    ...node.settings,
    node: {
      resolvePaths: [__dirname],
      tryExtensions: [`.js`, `.json`, `.node`, `.ts`],
    },
    "import/resolver": {
      node: {
        extensions: [`.js`, `.json`, `.node`, `.ts`],
      },
    },
  },
  rules: {
    ...node.rules,
    "node/no-unsupported-features/es-syntax": [
      `error`,
      { ignores: [`modules`] },
    ],
    quotes: `off`,
    "@typescript-eslint/quotes": [`error`, `backtick`],
    "no-unused-vars": `off`,
    "@typescript-eslint/no-unused-vars": [`error`, { args: `none` }],
  },
};
