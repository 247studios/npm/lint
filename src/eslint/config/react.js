/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */

const browser = require(`./browser.js`);
const insert = require(`../utilities/insert`);

module.exports = {
  ...browser,
  parser: `babel-eslint`,
  parserOptions: {
    ...browser.parserOptions,
    ecmaFeatures: {
      jsx: true,
    },
  },
  extends: insert(browser.extends, [
    [
      [
        `plugin:react/recommended`,
        `plugin:react-hooks/recommended`,
        `plugin:jsx-a11y/recommended`,
      ],
      `before`,
      `plugin:prettier/recommended`,
    ],
    [`prettier/react`, `after`, `plugin:prettier/recommended`],
    [`prettier/babel`, `end`],
  ]),
  settings: {
    react: {
      createClass: `createReactClass`,
      pragma: `React`,
      version: `latest`,
    },
    propWrapperFunctions: [
      `forbidExtraProps`,
      { property: `freeze`, object: `Object` },
    ],
    linkComponents: [{ name: `Link`, linkAttribute: `to` }],
  },
  rules: {
    ...browser.rules,
    quotes: [`error`, `backtick`],
  },
};
