/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */

const base = require(`./base.js`);
const insert = require(`../utilities/insert`);

module.exports = {
  ...base,
  env: {
    es6: true,
    node: true,
  },
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: `module`,
  },
  extends: insert(base.extends, [
    [`plugin:node/recommended`, `after`, `eslint:recommended`],
  ]),
  rules: {
    ...base.rules,
    quotes: [`error`, `backtick`],
  },
};
