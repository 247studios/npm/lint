/**
 * Copyright (c) 2020-present, 247studios Inc. All rights reserved.
 *
 * This file is proprietary and confidential.
 * Unauthorized copying of this file via any medium is strictly prohibited.
 */

const fs = require(`fs`);
const path = require(`path`);
const escapeRegExp = (x) => x.replace(/[.*+?^${}()|[\]\\]/g, `\\$&`);

const copyright = fs.readFileSync(
  path.resolve(__dirname, `../../copyright.txt`),
  `utf8`
);
const copyrightLines = copyright.split(`\n`);
const copyrightHeader = [
  `*`,
  ...copyrightLines.map((x) => (x ? ` * ${x}` : ` *`)),
  ` `,
].join(`\n`);
const copyrightPattern = [``, ...copyrightLines, ``]
  .map(escapeRegExp)
  .join(`\\s+\\*?\\s+`);

module.exports = {
  extends: [
    `eslint:recommended`,
    `plugin:import/errors`,
    `plugin:import/warnings`,
    `plugin:json/recommended`,
    `plugin:prettier/recommended`,
  ],
  plugins: [`header`],
  rules: {
    "header/header": [
      `error`,
      `block`,
      {
        pattern: copyrightPattern,
        template: copyrightHeader,
      },
    ],
    "no-unused-vars": [`error`, { args: `none` }],
  },
  globals: {
    __DEV__: `readonly`,
    process: `readonly`,
  },
  overrides: [
    {
      files: [`*.json`],
      rules: {
        "header/header": `off`,
      },
    },
  ],
};
